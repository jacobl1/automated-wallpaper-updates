#!/usr/local/bin/python3
import os
import time
import cv2
import re
import requests
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from random import randrange
import numpy as np
import io

def clear_images():
    '''
    Clears the wallpaper directory of all old jpgs 
    '''
    os.system("rm ~/wallpaper/*.jpg")

def get_dates(delta):
    '''
    The site uses this URL to host the https://apod.nasa.gov/apod/image/2201/PIA19048europa.jpg, so we need to calculate the date in YMd format.
    '''
    today = datetime.today() - timedelta(days=delta)
    date = today.strftime('%y%m%d')
    return date

def find_image(site):
    """
    God bless this library
    """
    soup = BeautifulSoup(requests.get(site).text, 'html.parser')
    img_tag = soup.find('img')
    img_url = img_tag.get('src')
    description = soup.find_all('b')[0].text
    return img_url,description

def get_image(img_dir,site,date):
    '''
    Gets the space photo from the NASA site
    '''
    img_url = f"{site.replace(f'ap{date}.html','')}{img_dir}"
    space_photo = requests.get(img_url,stream=True).content
    
    space_photo = io.BytesIO(space_photo)
    space_photo.decode_content = True
    image = np.asarray(bytearray(space_photo.read()), dtype="uint8")
    return image

def up_scale(space_photo,date,description):
    '''
    Uses opencv to upscale the image to and write their description
    '''
    scale_percent = 100
    img = cv2.imdecode(space_photo, cv2.IMREAD_COLOR)
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    resized = cv2.resize(img, dim, interpolation = cv2.INTER_LINEAR)
    textsize = cv2.getTextSize(description, cv2.FONT_HERSHEY_SIMPLEX, 1, 2)[0]
    textX = (resized.shape[1] - textsize[0]) / 2
    textY = (resized.shape[0] + textsize[1]) / 2
    org = (int(textX), int(1.75*textY))
    cv2.putText(resized, description, org,cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
    cv2.imwrite(f'~/wallpaper/{date}_up.jpg',resized) 

def main():
    clear_images()
    displays = ["first", "second", "third"]
    for display in displays:
        x = randrange(0,2000)
        date = get_dates(x)
        site = f'https://apod.nasa.gov/apod/ap{date}.html'
        img_url,description = find_image(site)
       
        space_photo = get_image(img_url,site,date)
     
        up_scale(space_photo,date,description)
        os.system(f"osascript -e 'tell application \"System Events\" to set picture of {display} desktop to \"~/wallpaper/Cosmo_Kramer.jpeg\"'") # for the memes
        time.sleep(.5)
        os.system(f"osascript -e 'tell application \"System Events\" to set picture of {display} desktop to \"~/wallpaper/{date}_up.jpg\"'") #update the desktop backround for each monitor

if __name__ == "__main__":
    main()


